package ca.mobile.jenovaprojects.rewardsreader.main.models;

/**
 * Created by jenova on 5/30/17.
 */

public class Points {

    public int points;
    public String item;
    public String time;

    public Points(){}

    public Points(int points, String item, String time) {
        this.points = points;
        this.item = item;
        this.time = time;
    }

    public int getPoints(){
        return points;
    }

    public void setPoints(int points){
        this.points = points;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTime() {
        return time;
    }

    public void setTime( String time) {
        this.time = time;
    }
}
