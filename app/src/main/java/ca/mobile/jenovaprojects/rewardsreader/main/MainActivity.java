package ca.mobile.jenovaprojects.rewardsreader.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ca.mobile.jenovaprojects.rewardsreader.R;
import ca.mobile.jenovaprojects.rewardsreader.main.models.Points;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    Context mContext;

    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    private String mUserToken, mUserName;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabase;
    private FirebaseAuth auth;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseInstanceId mFirebaseInstId;


    String time;

    int pointsInt;
    SurfaceView cameraView;
    TextView barcodeInfo, currentPoints, item;
    Button redeemCoffee, redeemSpec, redeemLunch, redeemDinner, cof, spec, lunch, dinner, btn1,
            btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, loginBtn, btnClear, btnRedeem;
    EditText amount;

    String barcodeValue, mUserId, mPoints, mItem, uid;
    int mPointsConverted, lunchPoints, bevPoints, dinnerPoints;

    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mFirebaseInstId = FirebaseInstanceId.getInstance();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            //loadLogInView();
        } else {
            mUserId = mFirebaseUser.getUid();
            mUserToken = mFirebaseInstId.getToken();
            mUserName = mFirebaseUser.getDisplayName();

            //loginBtn.setVisibility(View.GONE);


        }

        MobileAds.initialize(this, getString(R.string.google_ad_app_id));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.transition_ad_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {

        } else {
            requestCameraPermission();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            }
        });

        btnRedeem = (Button) findViewById(R.id.redeem);
        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPoints = amount.getText().toString();
                mPointsConverted = Integer.parseInt(mPoints);
                mUserId = barcodeInfo.getText().toString();
                mItem = item.getText().toString();
                Calendar cc = Calendar.getInstance();
                Date date = cc.getTime();
                // SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
                SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                time = format2.format(date);
                Points points = new Points(mPointsConverted*2, mItem, time);
                mDatabase.child("user").child(mUserId).push().setValue(points);
               /* if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }*/

                Restart();
            }
        });
        btnClear = (Button) findViewById(R.id.clear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.getText().clear();
            }
        });
        btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "1");
            }
        });
        btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "2");
            }
        });
        btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "3");
            }
        });
        btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "4");
            }
        });
        btn5 = (Button) findViewById(R.id.btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "5");
            }
        });
        btn6 = (Button) findViewById(R.id.btn6);
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "6");
            }
        });
        btn7 = (Button) findViewById(R.id.btn7);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "7");
            }
        });
        btn8 = (Button) findViewById(R.id.btn8);
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "8");
            }
        });
        btn9 = (Button) findViewById(R.id.btn9);
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "9");
            }
        });
        btn0 = (Button) findViewById(R.id.btn0);
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amount.setText(amount.getText() + "0");
            }
        });
        cameraView = (SurfaceView) findViewById(R.id.camera_view);
        barcodeInfo = (TextView) findViewById(R.id.code_info);
        currentPoints = (TextView) findViewById(R.id.currentPointsTv);
        item = (TextView) findViewById(R.id.itemTV);

        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        redeemCoffee = (Button) findViewById(R.id.coffBtn);
        redeemSpec = (Button) findViewById(R.id.specialBtn);
        redeemLunch = (Button) findViewById(R.id.lunchBtn);
        redeemDinner = (Button) findViewById(R.id.dinnerBtn);
        cof = (Button) findViewById(R.id.coffeeBtn);
        spec = (Button) findViewById(R.id.specBtn);
        lunch = (Button) findViewById(R.id.lunch_Btn);
        dinner = (Button) findViewById(R.id.dinner_Btn);
        amount = (EditText) findViewById(R.id.amountTv);

        cof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               item.setText("Coffee");
            }
        });

        spec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setText("Specialty");
            }
        });

        lunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setText("Lunch");
            }
        });

        dinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setText("Dinner");
            }
        });

       /* sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPoints = amount.getText().toString();
                mPointsConverted = Integer.parseInt(mPoints);
                mUserId = barcodeInfo.getText().toString();
                mItem = item.getText().toString();
                Points points = new Points(mPointsConverted*2, mItem);
                mDatabase.child("user").child(mUserId).push().setValue(points);
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }

                Restart();
            }
        }); */

        redeemCoffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redeemCoffeeTask();

            }
        });

        redeemSpec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               redeemSpecTask();
            }
        });

        redeemLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               redeemLunchTask();
            }
        });

        redeemDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               redeemDinnerTask();
            }
        });

        barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.QR_CODE)
                        .build();

        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(800, 600)
                .setAutoFocusEnabled(true)
                .build();

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    barcodeInfo.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                           /* barcodeInfo.setText(    // Update the TextView
                                    barcodes.valueAt(0).displayValue
                            );*/
                            barcodeValue = barcodes.valueAt(0).displayValue.toString();
                            barcodeInfo.setText(barcodeValue);
                            checkPoints();

                           /* Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                            intent.putExtra(barcodeValue, uid);
                            startActivity(intent);*/
                        }
                    });
                }
            }
        });

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @SuppressLint("MissingPermission")
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    cameraSource.start(cameraView.getHolder());
                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();

            }
        });

    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    public void Restart()
    {
        this.recreate();
        amount.getText().clear();
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // we have permission, so create the camerasource
            //boolean autoFocus = getIntent().getBooleanExtra(AutoFocus,false);
            //boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
            //createCameraSource(autoFocus, useFlash);
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Multitracker sample")
                .setMessage("We need permission")
                .setPositiveButton("Ok", listener)
                .show();
    }

    private void checkPoints() {
        mUserId = barcodeInfo.getText().toString();
        DatabaseReference db_receipt = FirebaseDatabase.getInstance().getReference().child("user").child(mUserId);


        db_receipt.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long sum = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Points points = ds.getValue(Points.class);
                    sum = sum + points.getPoints();
                }
                currentPoints.setText((int) sum + "");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void redeemCoffeeTask() {
        pointsInt = Integer.parseInt(currentPoints.getText().toString());
        if (pointsInt >= 65) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Redeem Coffee");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("Confirm",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mUserId = barcodeInfo.getText().toString();
                            bevPoints = -65;
                            mItem = "Redeem Coffee";
                            Calendar cc = Calendar.getInstance();
                            Date date = cc.getTime();
                            // SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
                            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                            time = format2.format(date);
                            Points points = new Points(bevPoints, mItem, time);
                            mDatabase.child("user").child(mUserId).push().setValue(points);

                            Restart();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(),"Not enough points to redeem :(",Toast.LENGTH_LONG).show();
            Restart();
        }
    }

    public void redeemSpecTask() {
        pointsInt = Integer.parseInt(currentPoints.getText().toString());
        if (pointsInt >= 115) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Redeem Specialty");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("Confirm",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mUserId = barcodeInfo.getText().toString();
                            bevPoints = -115;
                            mItem = "Redeem Specialty";
                            Calendar cc = Calendar.getInstance();
                            Date date = cc.getTime();
                            // SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
                            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                            time = format2.format(date);
                            Points points = new Points(bevPoints, mItem, time);
                            mDatabase.child("user").child(mUserId).push().setValue(points);

                            Restart();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(),"Not enough points to redeem :(",Toast.LENGTH_LONG).show();
            Restart();
        }
    }

    public void redeemLunchTask() {
        pointsInt = Integer.parseInt(currentPoints.getText().toString());
        if (pointsInt >= 275) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Redeem Lunch");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("Confirm",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mUserId = barcodeInfo.getText().toString();
                            lunchPoints = -275;
                            mItem = "Redeem Lunch";
                            Calendar cc = Calendar.getInstance();
                            Date date = cc.getTime();
                            // SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
                            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                            time = format2.format(date);
                            Points points = new Points(lunchPoints, mItem, time);
                            mDatabase.child("user").child(mUserId).push().setValue(points);

                            Restart();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(),"Not enough points to redeem :(",Toast.LENGTH_LONG).show();
            Restart();
        }
    }

    public void redeemDinnerTask() {
        pointsInt = Integer.parseInt(currentPoints.getText().toString());
        if (pointsInt >= 425) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setTitle("Redeem Dinner");
            builder.setMessage("Are you sure?");
            builder.setPositiveButton("Confirm",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mUserId = barcodeInfo.getText().toString();
                            dinnerPoints = -425;
                            mItem = "Redeem Dinner";
                            Calendar cc = Calendar.getInstance();
                            Date date = cc.getTime();
                            // SimpleDateFormat format1 = new SimpleDateFormat("dd MMM");
                            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
                            time = format2.format(date);
                            Points points = new Points(dinnerPoints, mItem, time);
                            mDatabase.child("user").child(mUserId).push().setValue(points);

                            Restart();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(getApplicationContext(),"Not enough points to redeem :(",Toast.LENGTH_LONG).show();
            Restart();
        }
    }

}
