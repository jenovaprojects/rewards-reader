package ca.mobile.jenovaprojects.rewardsreader.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import ca.mobile.jenovaprojects.rewardsreader.R;

public class RegisterActivity extends AppCompatActivity {

    FirebaseInstanceId mFireId;
    FirebaseUser mFireUser;

    String uid;
    TextView uId;
    EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Bundle bundle = getIntent().getExtras();
        String mUid = bundle.getString(uid);

        uId = (TextView) findViewById(R.id.uidTV);
        email = (EditText) findViewById(R.id.emailTV);
        uId.setText(uid);
    }

}
